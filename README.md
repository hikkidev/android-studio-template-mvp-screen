# [Устарел] Android Studio Template - MVP Screen

Шаблон для создания MVP экрана

Создает следующие файлы:
  - Interface: `{name}View.kt`
  - Class: `{name}Presenter.kt`
  - Class: `{name}Fragment.kt`

Зависит от [core-mvp](https://gitlab.com/hikkidev/android-modules/tree/master/core-mvp) модуля и добавлет в зависимости (если его нет)
# Установка

1. Скопировать папку шаблона в `{path to Android Studio}\plugins\android\lib\templates\other`
2. Перезапустить студию (если она была открыта)

# Использование

Right Click or File -> New -> Custom -> MVP Screen  
`Ctrl + Shift + A` -> MVP Screen
