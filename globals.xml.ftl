<?xml version="1.0"?>
<globals>
	<#include "root://activities/common/common_globals.xml.ftl" />
    <global id="manifestOut" value="${manifestDir}" />
    <global id="srcOut" value="${srcDir}/${slashedPackageName(packageName)}" />
    <global id="resOut" value="${resDir}" />
    
    <global id="defPostfixFragment" value="Fragment" />
    <global id="defPostfixPresenter" value="Presenter" />
    <global id="defPostfixView" value="View" />
</globals>
