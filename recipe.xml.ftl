<?xml version="1.0"?>
<#import "root://activities/common/kotlin_macros.ftl" as kt>
<recipe>
	<@kt.addAllKotlinDependencies />
    <#if !(hasDependency('com.hikkideveloper:core-mvp'))>
        <dependency mavenUrl="com.hikkideveloper:core-mvp:1.0.2"/>
    </#if>
    
     <instantiate from="src/app_package/Interface.kt.ftl"
             to="${escapeXmlAttribute(srcOut)}/${nameScreen}${defPostfixView}.kt" />

    <instantiate from="src/app_package/Presenter.kt.ftl"
             to="${escapeXmlAttribute(srcOut)}/${nameScreen}${defPostfixPresenter}.kt" />

    <instantiate from="src/app_package/Fragment.kt.ftl"
             to="${escapeXmlAttribute(srcOut)}/${nameScreen}${defPostfixFragment}.kt" />

    <#if generateLayout>
    <instantiate from="res/layout/layout.xml.ftl"
                   to="${escapeXmlAttribute(resOut)}/layout/${nameRes}.xml" />
    </#if>

    
    <open file="${escapeXmlAttribute(srcOut)}/${nameScreen}${defPostfixFragment}.kt" />
</recipe>