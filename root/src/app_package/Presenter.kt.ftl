package ${escapeKotlinIdentifiers(packageName)}

import com.arellomobile.mvp.InjectViewState
import com.hikkideveloper.core.mvp.navigation.NavigationManager
import com.hikkideveloper.core.mvp.presenter.BasePresenter
import toothpick.InjectConstructor

@InjectViewState
@InjectConstructor
class ${nameScreen}Presenter(navigationManager: NavigationManager) : BasePresenter<${nameScreen}View>(navigationManager) {

    override fun handleCoroutineException(throwable: Throwable) {
        // TODO Stub auto generation 
        viewState.showToast(throwable.localizedMessage ?: "CoroutineException")
        throwable.printStackTrace()
    }
}