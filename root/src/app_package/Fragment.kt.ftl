package ${escapeKotlinIdentifiers(packageName)}


<#if !generateLayout>
import android.view.View
</#if>
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hikkideveloper.core.mvp.LayoutId
import com.hikkideveloper.core.mvp.fragment.BaseFragment
import com.hikkideveloper.core.mvp.navigation.BackListener
import toothpick.Scope
import toothpick.Toothpick
<#if generateLayout>
<#if applicationPackage??>
import ${applicationPackage}.R
</#if>

@LayoutId(R.layout.${nameRes})
<#else>

@LayoutId
</#if>
class ${nameScreen}${defPostfixFragment} : BaseFragment(), ${nameScreen}${defPostfixView}, BackListener {

    // TODO Stub auto generation 
    override val scope: Scope = Toothpick.openRootScope()

    @InjectPresenter
    lateinit var presenter: ${nameScreen}${defPostfixPresenter}

    @ProvidePresenter
    fun providePresenter(): ${nameScreen}${defPostfixPresenter} = scope.getInstance(${nameScreen}${defPostfixPresenter}::class.java)

    override fun onBack(): Boolean {
        TODO("If the fragment will intercept OnBackPressed -> call presenter.back(RouterName) and return true else false.")
    }

    <#if !generateLayout>
    override fun createView(): View {
        // TODO Create your view here
        return super.createView()
    }
    </#if>

    companion object {

        @JvmStatic 
        fun newInstance() = ${nameScreen}${defPostfixFragment}()
    }
}
