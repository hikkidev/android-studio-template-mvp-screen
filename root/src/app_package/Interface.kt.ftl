package ${escapeKotlinIdentifiers(packageName)}

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.hikkideveloper.core.mvp.view.BaseView


@StateStrategyType(AddToEndSingleStrategy::class)
interface ${nameScreen}${defPostfixView} : BaseView {

}
